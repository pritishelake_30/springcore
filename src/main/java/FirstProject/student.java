package FirstProject;
public class student 
{
	private int studentId;
	private String studentname;
	private String studentAddress;
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		System.out.println("Setting student Id");
		this.studentId = studentId;
	}
	public String getStudentname() {
		
		return studentname;
	}
	public void setStudentname(String studentname) {
		System.out.println("Setting student Name");
		this.studentname = studentname;
	}
	public String getStudentAddress() {
		
		return studentAddress;
	}
	public void setStudentAddress(String studentAddress) {
		System.out.println("Setting student Address");
		this.studentAddress = studentAddress;
	}
	public student(int studentId, String studentname, String studentAddress) {
		super();
		this.studentId = studentId;
		this.studentname = studentname;
		this.studentAddress = studentAddress;
	}
	public student() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return " studentId=" + studentId + ", studentname=" + studentname + ", studentAddress=" + studentAddress
				;
	}
	
	

}
